package com.sda.sudoku;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;

public class SudokuChecker {
    private List<Set<Integer>> cols;
    private List<Set<Integer>> rows;
    private List<Set<Integer>> squares;

    public boolean check(int s[][]) {
        verifyGridSize(s);
        //prepare hashSets
        prepareData();
        //traverse each number once
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                //validate number
                int current = s[i][j];
                if (current < 1 || current > 9) {
                    return false;
                }
                int squareNumber = 3*(i/3)+(j/3);
                Set<Integer> column = cols.get(j);
                Set<Integer> row = rows.get(i);
                Set<Integer> square = squares.get(squareNumber);
                if(!(column.add(current)&&row.add(current)&&square.add(current))){
                    return false;
                }
            }
        }
        return true;
    }

    private void verifyGridSize(int s[][]) {
        if (s.length != 9) {
            throw new InvalidGridSizeException();
        } else {
            for (int i = 0; i < s.length; i++) {
                if (s[i] == null || s[i].length != 9) {
                    throw new InvalidGridSizeException();
                }
            }
        }
    }

    private void prepareData(){
        cols = generateEmptyList();
        rows = generateEmptyList();
        squares = generateEmptyList();
    }

    private ArrayList<Set<Integer>> generateEmptyList(){
        return Stream.generate(HashSet<Integer>::new).limit(9).collect(toCollection(ArrayList::new));
    }
}
