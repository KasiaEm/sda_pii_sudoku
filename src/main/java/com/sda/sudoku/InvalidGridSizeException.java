package com.sda.sudoku;

public class InvalidGridSizeException extends RuntimeException{
    public InvalidGridSizeException(){
        super("Invalid grid size.");
    }
}
