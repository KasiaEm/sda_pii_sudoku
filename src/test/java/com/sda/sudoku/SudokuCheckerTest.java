package com.sda.sudoku;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class SudokuCheckerTest {
    private SudokuChecker checker;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void init() {
        checker = new SudokuChecker();
    }

    @Test
    @Parameters(source = ValidGridProvider.class)
    public void checkValid(int grid[][]) {
        assertThat(checker.check(grid)).isTrue();
    }

    @Test
    @Parameters
    public void checkInvalidSize(int grid[][]){
        thrown.expect(InvalidGridSizeException.class);
        checker.check(grid);
    }

    private int[][][] parametersForCheckInvalidSize(){
        return new int[][][]{
                new int[9][2],
                new int[1][1]
        };
    }

    @Test
    @Parameters(source = InvalidGridProvider.class)
    public void checkInvalid(int grid[][]){
        assertThat(checker.check(grid)).isFalse();
    }
}